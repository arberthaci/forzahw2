package com.arberthaci.forzafootballhw2.http;

import com.arberthaci.forzafootballhw2.http.model.Team;

import java.util.ArrayList;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Arbër Thaçi on 18-02-08.
 * Email: arberlthaci@gmail.com
 */

public interface TeamsApi {

    @GET("teams.json")
    Observable<ArrayList<Team>> getTeams();

}
