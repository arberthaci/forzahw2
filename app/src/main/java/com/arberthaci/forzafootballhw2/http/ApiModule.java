package com.arberthaci.forzafootballhw2.http;

import com.arberthaci.forzafootballhw2.http.model.Team;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

/**
 * Created by Arbër Thaçi on 18-02-08.
 * Email: arberlthaci@gmail.com
 */

public class ApiModule implements TeamsApi {

    private TeamsApi api;
    public static final String BASE_URL = "https://s3-eu-west-1.amazonaws.com/forza-assignment/android/";

    public OkHttpClient provideClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

    public ApiModule(String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(provideClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        this.api = retrofit.create(TeamsApi.class);
    }

     @Override
     public Observable<ArrayList<Team>> getTeams() {
        return this.api.getTeams();
    }

}
