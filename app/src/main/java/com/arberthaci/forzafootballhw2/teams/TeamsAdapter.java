package com.arberthaci.forzafootballhw2.teams;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arberthaci.forzafootballhw2.R;
import com.arberthaci.forzafootballhw2.http.model.Team;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Arbër Thaçi on 18-02-08.
 * Email: arberlthaci@gmail.com
 */

public class TeamsAdapter extends RecyclerView.Adapter<TeamsAdapter.TeamsViewHolder> {

    private Context mContext;
    private ArrayList<Team> itemsList = new ArrayList<>();

    public class TeamsViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvNationality;
        ImageView ivThumbnail;
        TextView btnSeeMore;

        TeamsViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.team_name);
            tvNationality = view.findViewById(R.id.team_nationality);
            ivThumbnail = view.findViewById(R.id.team_logo);
            btnSeeMore = view.findViewById(R.id.team_see_more);

            mContext = view.getContext();
        }
    }

    public TeamsAdapter(ArrayList<Team> itemsList) {
        this.itemsList = itemsList;
    }

    public void setItemsList(ArrayList<Team> itemsList) {
        this.itemsList = itemsList;
        notifyDataSetChanged();
    }

    public void add(Team mTeam) {
        itemsList.add(mTeam);
        notifyDataSetChanged();
    }

    @Override
    public TeamsAdapter.TeamsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_rv_team, parent, false);
        return new TeamsAdapter.TeamsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TeamsAdapter.TeamsViewHolder holder, final int position) {
        Team teamObject = itemsList.get(position);

        holder.tvName.setText(teamObject.getName());
        holder.tvNationality.setText(teamObject.getCountryName());

        switch (teamObject.getName()) {
            case "Arsenal FC":
                loadImage(holder.ivThumbnail, R.drawable.arsenal);
                break;
            case "FC Barcelona":
                loadImage(holder.ivThumbnail, R.drawable.barcelona);
                break;
            case "Sweden":
                loadImage(holder.ivThumbnail, R.drawable.sweden);
                break;
            case "Inter Milan":
                loadImage(holder.ivThumbnail, R.drawable.inter_milan);
                break;
        }
    }

    @Override
    public int getItemCount() {
        if(itemsList != null && itemsList.size() != 0) {
            return itemsList.size();
        }
        return 0;
    }

    private void loadImage(ImageView imageView, int drawable) {
        Picasso.with(mContext)
                .load(drawable)
                .placeholder(R.drawable.image_not_available_placeholder)
                .error(R.drawable.image_not_available_placeholder)
                .into(imageView);
    }
}