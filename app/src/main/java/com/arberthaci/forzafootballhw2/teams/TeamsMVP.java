package com.arberthaci.forzafootballhw2.teams;

import com.arberthaci.forzafootballhw2.http.model.Team;

import java.util.ArrayList;

/**
 * Created by Arbër Thaçi on 18-02-08.
 * Email: arberlthaci@gmail.com
 */

public interface TeamsMVP {

    interface View {

        void onFetchDataStarted();

        void onFetchDataCompleted();

        void onFetchDataSuccess(ArrayList<Team> teamsResponse);

        void onFetchDataError(Throwable e);
    }

    interface Presenter {

        void loadData();

        void subscribe();

        void unSubscribe();
    }

}
