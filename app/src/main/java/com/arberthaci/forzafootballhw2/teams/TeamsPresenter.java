package com.arberthaci.forzafootballhw2.teams;

import android.support.annotation.NonNull;

import com.arberthaci.forzafootballhw2.http.TeamsApi;
import com.arberthaci.forzafootballhw2.http.model.Team;

import java.util.ArrayList;

import rx.Observer;
import rx.Scheduler;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Arbër Thaçi on 18-02-08.
 * Email: arberlthaci@gmail.com
 */

public class TeamsPresenter implements TeamsMVP.Presenter {

    @NonNull
    private TeamsApi teamsApi;
    @NonNull
    private Scheduler backgroundScheduler;
    @NonNull
    private Scheduler mainScheduler;
    @NonNull
    private CompositeSubscription subscriptions;
    private TeamsMVP.View view;

    public TeamsPresenter(
            @NonNull TeamsApi teamsApi,
            @NonNull Scheduler backgroundScheduler,
            @NonNull Scheduler mainScheduler,
            TeamsMVP.View view) {
        this.teamsApi = teamsApi;
        this.backgroundScheduler = backgroundScheduler;
        this.mainScheduler = mainScheduler;
        this.view = view;
        subscriptions = new CompositeSubscription();
    }

    @Override
    public void loadData() {
        view.onFetchDataStarted();
        subscriptions.clear();

        Subscription subscription = teamsApi
                .getTeams()
                .subscribeOn(backgroundScheduler)
                .observeOn(mainScheduler)
                .subscribe(new Observer<ArrayList<Team>>() {
                    @Override
                    public void onCompleted() {
                        view.onFetchDataCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onFetchDataError(e);
                    }

                    @Override
                    public void onNext(ArrayList<Team> teams) {
                        view.onFetchDataSuccess(teams);
                    }
                });

        subscriptions.add(subscription);
    }

    @Override
    public void subscribe() {
        loadData();
    }

    @Override
    public void unSubscribe() {
        subscriptions.clear();
    }
}
