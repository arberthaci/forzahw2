package com.arberthaci.forzafootballhw2.teams;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.arberthaci.forzafootballhw2.R;
import com.arberthaci.forzafootballhw2.http.ApiModule;
import com.arberthaci.forzafootballhw2.http.model.Team;

import java.util.ArrayList;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Arbër Thaçi on 18-02-08.
 * Email: arberlthaci@gmail.com
 */

public class MainActivity extends AppCompatActivity implements TeamsMVP.View {

    private TeamsMVP.Presenter presenter;

    private RecyclerView rvTeams;
    private ProgressBar pbLoading;
    private TeamsAdapter adapter;
    private ArrayList<Team> teamsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeComponents();
        afterInitialization();
        initializePresenter();

        presenter.subscribe();
    }

    private void initializeComponents() {
        rvTeams = findViewById(R.id.rv_teams);
        pbLoading = findViewById(R.id.pb_loading);
    }

    private void afterInitialization() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvTeams.setLayoutManager(mLayoutManager);
        rvTeams.setItemAnimator(new DefaultItemAnimator());
        rvTeams.setHasFixedSize(false);

        adapter = new TeamsAdapter(teamsList);
        rvTeams.setAdapter(adapter);
    }

    private void initializePresenter() {
        presenter = new TeamsPresenter(
                new ApiModule(ApiModule.BASE_URL),
                Schedulers.io(),
                AndroidSchedulers.mainThread(),
                this);
    }


    @Override
    public void onFetchDataStarted() {
        pbLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFetchDataCompleted() {
        pbLoading.setVisibility(View.GONE);
        presenter.unSubscribe();
    }

    @Override
    public void onFetchDataSuccess(ArrayList<Team> teamsResponse) {
        adapter.setItemsList(teamsResponse);
    }

    @Override
    public void onFetchDataError(Throwable e) {
        Toast.makeText(MainActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
    }
}
