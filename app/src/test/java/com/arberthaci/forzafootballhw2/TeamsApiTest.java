package com.arberthaci.forzafootballhw2;

import com.arberthaci.forzafootballhw2.http.ApiModule;
import com.arberthaci.forzafootballhw2.http.model.Team;
import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import rx.observers.TestSubscriber;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Arbër Thaçi on 18-02-08.
 * Email: arberlthaci@gmail.com
 */

public class TeamsApiTest {

    private Gson gson;
    private String goodResponseJson = "[{\"name\":\"Arsenal FC\",\"national\":false,\"country_name\":\"England\"},{\"name\":\"FC Barcelona\",\"national\":false,\"country_name\":\"Spain\"},{\"name\":\"Sweden\",\"national\":true,\"country_name\":\"Sweden\"},{\"name\":\"Inter Milan\",\"national\":false,\"country_name\":\"Italy\"}]";
    private String badResponseJson = "[{\"team\":\"Arsenal FC\",\"national\":\"England\"}]";
    private MockWebServer mMockWebServer;
    private TestSubscriber<ArrayList<Team>> mSubscriber;

    @Before
    public void setUp() {
        gson = new Gson();
        mMockWebServer = new MockWebServer();
        mSubscriber = new TestSubscriber<>();
    }

    /**
     * In this use case,
     * parsing of the response
     * SUCCESSFULLY passes the test
     **/
    @Test
    public void serverCallWithSuccessfulParsing() {
        MockResponse mMockResponse = new MockResponse()
                .setResponseCode(200)
                .setBody(goodResponseJson);
        mMockWebServer.enqueue(mMockResponse);
        ApiModule apiModule = new ApiModule(ApiModule.BASE_URL);

        apiModule.getTeams().subscribe(mSubscriber);

        mSubscriber.assertNoErrors();
        mSubscriber.assertCompleted();
        assertEquals(goodResponseJson, gson.toJson(mSubscriber.getOnNextEvents().get(0)));
    }

    /**
     * In this use case,
     * parsing of the response
     * FAIL to pass the test
     **/
    @Test
    public void serverCallWithErrorParsing() {
        MockResponse mMockResponse = new MockResponse()
                .setResponseCode(200)
                .setBody(badResponseJson);
        mMockWebServer.enqueue(mMockResponse);
        ApiModule apiModule = new ApiModule(ApiModule.BASE_URL);

        apiModule.getTeams().subscribe(mSubscriber);

        mSubscriber.assertNoErrors();
        mSubscriber.assertCompleted();
        assertEquals(badResponseJson, gson.toJson(mSubscriber.getOnNextEvents().get(0)));
    }

    /**
     * In this use case,
     * handling the correct endpoint
     * SUCCESSFULLY passes the test
     **/
    @Test
    public void serverCallForTestingEndpoint() throws Exception {
        MockResponse mMockResponse = new MockResponse()
                .setResponseCode(200)
                .setBody(goodResponseJson);
        mMockWebServer.enqueue(mMockResponse);
        mMockWebServer.start();

        HttpUrl baseUrl = mMockWebServer.url("");
        ApiModule apiModule = new ApiModule(baseUrl.toString());
        apiModule.getTeams().subscribe(mSubscriber);

        RecordedRequest request = mMockWebServer.takeRequest();
        assertEquals("/teams.json", request.getPath());

        mMockWebServer.shutdown();
    }

    /**
     * In this use case,
     * handling the correct request method
     * SUCCESSFULLY passes the test
     **/
    @Test
    public void serverCallForTestingRequestMethod() throws Exception {
        MockResponse mMockResponse = new MockResponse()
                .setResponseCode(200)
                .setBody(goodResponseJson);
        mMockWebServer.enqueue(mMockResponse);
        mMockWebServer.start();

        HttpUrl baseUrl = mMockWebServer.url("");
        ApiModule apiModule = new ApiModule(baseUrl.toString());
        apiModule.getTeams().subscribe(mSubscriber);

        RecordedRequest request = mMockWebServer.takeRequest();
        assertEquals("GET", request.getMethod());

        mMockWebServer.shutdown();
    }

    /**
     * In this use case,
     * handling the 200 response code
     * SUCCESSFULLY passes the test
     **/
    @Test
    public void serverCallWith200ResponseCode() throws Exception {
        MockResponse mMockResponse = new MockResponse()
                .setResponseCode(200)
                .setBody(goodResponseJson);

        mMockWebServer.enqueue(mMockResponse);
        mMockWebServer.start();

        HttpUrl baseUrl = mMockWebServer.url("");
        ApiModule apiModule = new ApiModule(baseUrl.toString());
        apiModule.getTeams().subscribe(mSubscriber);

        assertEquals("HTTP/1.1 200 OK", mMockResponse.getStatus());

        mMockWebServer.shutdown();
    }

    /**
     * In this use case,
     * handling the 404 response code
     * SUCCESSFULLY passes the test
     **/
    @Test
    public void serverCallWith404ResponseCode() throws Exception {
        MockResponse mMockResponse = new MockResponse()
                .setResponseCode(404)
                .setBody("{}");

        mMockWebServer.enqueue(mMockResponse);
        mMockWebServer.start();

        HttpUrl baseUrl = mMockWebServer.url("");
        ApiModule apiModule = new ApiModule(baseUrl.toString());
        apiModule.getTeams().subscribe(mSubscriber);

        assertEquals("HTTP/1.1 404 Client Error", mMockResponse.getStatus());

        mMockWebServer.shutdown();
    }

    /**
     * In this use case,
     * handling the 500 response code
     * SUCCESSFULLY passes the test
     **/
    @Test
    public void serverCallWith500ResponseCode() throws Exception {
        MockResponse mMockResponse = new MockResponse()
                .setResponseCode(500)
                .setBody("{}");

        mMockWebServer.enqueue(mMockResponse);
        mMockWebServer.start();

        HttpUrl baseUrl = mMockWebServer.url("");
        ApiModule apiModule = new ApiModule(baseUrl.toString());
        apiModule.getTeams().subscribe(mSubscriber);

        assertEquals("HTTP/1.1 500 Server Error", mMockResponse.getStatus());

        mMockWebServer.shutdown();
    }
}
