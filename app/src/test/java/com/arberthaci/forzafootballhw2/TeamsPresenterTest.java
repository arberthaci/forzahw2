package com.arberthaci.forzafootballhw2;

import com.arberthaci.forzafootballhw2.http.TeamsApi;
import com.arberthaci.forzafootballhw2.http.model.Team;
import com.arberthaci.forzafootballhw2.teams.TeamsMVP;
import com.arberthaci.forzafootballhw2.teams.TeamsPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import rx.Observable;
import rx.schedulers.Schedulers;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Arbër Thaçi on 18-02-08.
 * Email: arberlthaci@gmail.com
 */

public class TeamsPresenterTest {

    @Mock
    private TeamsApi teamsApi;
    @Mock
    private TeamsMVP.View view;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void fetchValidDataShouldLoadIntoView() {
        ArrayList<Team> teams = new ArrayList<>();
        when(teamsApi.getTeams()).thenReturn(Observable.just(teams));

        TeamsPresenter teamsPresenter = new TeamsPresenter(
                this.teamsApi,
                Schedulers.immediate(),
                Schedulers.immediate(),
                this.view
        );

        teamsPresenter.loadData();

        InOrder inOrder = Mockito.inOrder(view);
        inOrder.verify(view, times(1)).onFetchDataStarted();
        inOrder.verify(view, times(1)).onFetchDataSuccess(teams);
        inOrder.verify(view, times(1)).onFetchDataCompleted();
    }

    @Test
    public void fetchErrorShouldReturnErrorToView() {
        Exception exception = new Exception();

        when(teamsApi.getTeams()).thenReturn(Observable.<ArrayList<Team>>error(exception));

        TeamsPresenter teamsPresenter = new TeamsPresenter(
                this.teamsApi,
                Schedulers.immediate(),
                Schedulers.immediate(),
                this.view
        );

        teamsPresenter.loadData();

        InOrder inOrder = Mockito.inOrder(view);
        inOrder.verify(view, times(1)).onFetchDataStarted();
        inOrder.verify(view, times(1)).onFetchDataError(exception);
        verify(view, never()).onFetchDataCompleted();
    }

}
